#!/bin/bash

Destination="../Release"
BundleName="PanthonSlicer.app"

helpFunction()
{
   echo ""
   echo "Usage: $0 -a appleid -p app-specific password"
   exit 1 # Exit script after printing help
}

while getopts "a:p:" opt
do
   case "$opt" in
      a ) parameterA="$OPTARG" ;;
      p ) parameterB="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$parameterA" ] || [ -z "$parameterB" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

echo "Zipping files"

ditto -c -k --sequesterRsrc --keepParent "$Destination/$BundleName" "$Destination/PantheonSlicer.zip"
echo "Uploading files to apple servers..."
UUID=$(xcrun altool --notarize-app --username $parameterA --password $parameterB --asc-provider LXVCBJ7HN9  --primary-bundle-id Pantheon Slicer --file ../Release/PantheonSlicer.zip | grep  "RequestUUID = " | sed 's/RequestUUID = //')
echo "Waiting for reply..."
echo "RequestUUID = $UUID"
while true
do
    result=$(xcrun altool --notarization-info $UUID  --username $parameterA --password $parameterB  --asc-provider LXVCBJ7HN9)
    echo "$result"
    echo ""
    result=$(echo $result | grep -c "in progress")
    
    if [ $result -le 0 ]
    then
        break
    fi
    sleep 5
done