#!/bin/sh

Destination="../Release"
BundleName="PanthonSlicer.app"
SIGN_IDENTITY="Developer ID Application: Pantheon Design Ltd. (LXVCBJ7HN9)"

mkdir -p $Destination/$BundleName/Contents
mkdir -p $Destination/$BundleName/Contents/MacOS
mkdir -p $Destination/$BundleName/Contents/resources

cp $Destination/src/PrusaSlicer $Destination/$BundleName/Contents/MacOS/
cp -r $Destination/resources/* $Destination/$BundleName/Contents/resources/
cp ./Info.plist $Destination/$BundleName/Contents/
cp $Destination/$BundleName/Contents/resources/icons/PantheonSlicer.icns $Destination/$BundleName/Contents/resources/

echo ""
codesign  --options=runtime --force -s "$SIGN_IDENTITY" $Destination/$BundleName
echo ""
codesign -dv --verbose=2 $Destination/$BundleName
echo ""
codesign -vvv --deep --strict $Destination/$BundleName 

echo "\nDone! If the app bundle does not have an icon, click show contents on the context menu and go back, It's a bug in MacOS."
echo "to finalize and publish the bundle, notarize it first using notarize.sh -a APPLE_ID -p APP_SPECIFIC_PASSWORD"